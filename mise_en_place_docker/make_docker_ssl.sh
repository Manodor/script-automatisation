#!/bin/bash

if [ -z $1 ]
then
	echo "Please give me DNS docker will run on"
	exit
fi

if [ -z $2 ]
then
	echo "Please give me parent directory to create all certificate"
	exit
fi

HOST=$1

mkdir -p $2
cd $2

mkdir -p $2/server_cert
cd $2/server_cert

# CREATION DES CERTIFICATS
openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=$HOST" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:$HOST,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out server-cert.pem -extfile extfile.cnf
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr
echo extendedKeyUsage = clientAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out cert.pem -extfile extfile.cnf
rm -v client.csr server.csr
chmod -v 0400 ca-key.pem key.pem server-key.pem
chmod -v 0444 ca.pem server-cert.pem cert.pem

cd ..
mkdir -p $2/client_cert

cp $2/server_cert/ca.pem $2/client_cert/ca.pem
cp $2/server_cert/cert.pem $2/client_cert/cert.pem
cp $2/server_cert/key.pem $2/client_cert/key.pem

# FIN CREATION CERTIFICATS


echo "Please replace line $(cat /lib/systemd/system/docker.service | grep ExecStart)"
echo "BY:"
echo "ExecStart=/usr/bin/dockerd --tlsverify --tlscacert=$2/server_cert/ca.pem --tlscert=$2/server_cert/server-cert.pem --tlskey=$2/server_cert/server-key.pem -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock"

read confirm
nano /lib/systemd/system/docker.service

systemctl daemon-reload && systemctl restart docker
