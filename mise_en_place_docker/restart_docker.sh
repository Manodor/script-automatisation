#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

function restart() {
	docker restart ${@:1}
}

if [ -z $1 ]
then
	echo "Veuillez spécifiez au moins 1 parametre:"
	echo "	- Liste de container id (exemple: restart_docker.sh container_id_1 container_id_2 container_id_...)"
	echo "	- RUNNING: Relance tout les containers actuellement lancer"
	echo "	- ALL: Relance tout les containers, déjà lancer ou non"
	exit
fi

case $1 in
	"RUNNING")
		restart $(docker ps -q)
		;;
	"ALL")
		restart $(docker ps -aq)
		;;
	*)
		restart ${@:1}
		;;
esac
